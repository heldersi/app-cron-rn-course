import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

type Props = {};
export default class App extends Component<Props> {
  constructor(props){
    super(props);
    this.state = { 
      started: false, 
      paused: false, 
      seconds: 0,
      timerId: null,
    };
  }

  _start = () => {
    if(this.state.started) return null;
    const intervalId = setInterval(() => {
      if (this.state.paused) return null;
      let seconds = this.state.seconds + 1;
      this.setState( prev => {
        return { seconds }
      });
    }, 1000);

    this.setState( prev =>{
      return {timerId: intervalId, started: true };
    });
  }

  _pause = () =>{
    this.setState( prev => {
      return { paused: !prev.paused };
    })
  }

  _reset = () => {
    clearInterval(this.state.timerId);
    this.setState( prev => {
      return { seconds: 0, started: false };
    })
  }

  render() {
    let pause_resume_text = this.state.paused ? 'Resume' : 'Pause';

    return (
      <View style={styles.container}>
        <View style={styles.visor}>
          <Text style={ styles.seconds } >{this.state.seconds}</Text>
        </View>
        <View style={ styles.actions }>
          <TouchableOpacity style={ this.state.started ? styles.disabled : styles.button } onPress={ () => this._start() } disabled={this.state.started}>
            <Text style={ styles.buttonText }>Start</Text>
          </TouchableOpacity>
          <TouchableOpacity style={ styles.button } onPress={ () => this._pause() }>
            <Text style={ styles.buttonText }>{pause_resume_text}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={ styles.button } onPress={ () => this._reset() }>
            <Text style={ styles.buttonText }>Reset</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  button: {
    flex: 1,
    backgroundColor: '#7A91CA',
    margin: 10,
    borderRadius: 5,
  },
  disabled:{
    flex: 1,
    backgroundColor: '#EEE',
    margin: 10,
    borderRadius: 5,
  },
  buttonText:{
    color: 'rgba(0, 0, 0, 0.5)',
    margin: 20,
    alignSelf: 'center',
    fontSize: 15
  },
  visor:{
    flex: 5,
    backgroundColor: '#EEE',
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  seconds:{
    fontSize: 100,
  },
  actions:{
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  }
});
